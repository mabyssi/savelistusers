package com.mabyssi28.saveNames.controller;

import com.mabyssi28.saveNames.model.User;
import com.mabyssi28.saveNames.service.IUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/user")
public class UserController {
    private final IUserService userService;

    @PostMapping(value = "/save")
    public void saveUser(
            @RequestBody User user
    ) { this.userService.saveUser(user); }

    @GetMapping(value = "/get-all")
    public List<User> getAllUsers() { return this.userService.getAllUsernames(); }

    @DeleteMapping(value = "/{id}")
    public boolean deleteUserById(
            @PathVariable Long id
    ) { return this.userService.deleteUsernameById(id); }
}
