package com.mabyssi28.saveNames.repository;

import com.mabyssi28.saveNames.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserRepository extends JpaRepository<User, Long> { }
