package com.mabyssi28.saveNames;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SaveNamesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SaveNamesApplication.class, args);
	}

}
