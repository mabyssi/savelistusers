package com.mabyssi28.saveNames.service;

import com.mabyssi28.saveNames.model.User;

import java.util.List;

public interface IUserService {
    boolean saveUser(User user);

    boolean deleteUsernameById(Long id);

    List<User> getAllUsernames();
}
