package com.mabyssi28.saveNames.service;

import com.mabyssi28.saveNames.model.User;
import com.mabyssi28.saveNames.repository.IUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService implements IUserService {
    private final IUserRepository userRepository;

    @Override
    public boolean saveUser(User user) {
        this.userRepository.save(user);

        return true;
    }

    @Override
    public boolean deleteUsernameById(Long id) {
        this.userRepository.deleteById(id);

        return true;
    }

    @Override
    public List<User> getAllUsernames() {
        return this.userRepository.findAll();
    }
}
